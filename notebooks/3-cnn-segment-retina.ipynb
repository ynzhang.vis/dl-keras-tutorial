{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Training a convolution neural network to segment retina image\n",
    "\n",
    "Trains a UNet on the [DRIVE dataset](http://www.isi.uu.nl/Research/Databases/DRIVE/) [Staal et al, 2004] to segment retinal images.\n",
    "\n",
    "J.J. Staal, M.D. Abramoff, M. Niemeijer, M.A. Viergever, B. van Ginneken, \"[Ridge based vessel segmentation in color images of the retina](http://www.isi.uu.nl/Research/Databases/DRIVE/id=855.html)\", IEEE Transactions on Medical Imaging, 2004, vol. 23, pp. 501-509."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pylab as plt\n",
    "import numpy as np\n",
    "import os, glob\n",
    "import pandas as pd # load csv\n",
    "import zipfile # extract zip\n",
    "from skimage.external import tifffile # read tiff images\n",
    "from skimage.io import imread # read gif images\n",
    "from skimage.transform import resize # resize images"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Preparing the data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Loading the image data__\n",
    "\n",
    "The image data will be imported as tensors of shape: NUM x WIDTH x HEIGHT.\n",
    "\n",
    "The labels will be imported as tensors of shape: NUM x 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# unzip data set\n",
    "with zipfile.ZipFile(\"../data/DRIVE_retinal_image_data.zip\",\"r\") as zip_ref:\n",
    "    zip_ref.extractall(\"../data\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load training images\n",
    "fns = sorted(glob.glob('../data/DRIVE/training/images/*.tif'))\n",
    "#print(fns)\n",
    "x_train = np.array([tifffile.imread(fn) for fn in fns])\n",
    "\n",
    "# load test images\n",
    "fns = sorted(glob.glob('../data/DRIVE/test/images/*.tif'))\n",
    "#print(fns)\n",
    "x_test = np.array([tifffile.imread(fn) for fn in fns])\n",
    "print(x_train.shape)\n",
    "print(x_test.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load training annotations\n",
    "fns = sorted(glob.glob('../data/DRIVE/training/labels/*.gif'))\n",
    "y_train = np.array([imread(fn) for fn in fns]) # read images\n",
    "y_train = np.expand_dims(y_train, -1) # add channels dimension\n",
    "\n",
    "# load test annotations\n",
    "fns = sorted(glob.glob('../data/DRIVE/test/labels/*.gif'))\n",
    "y_test = np.array([imread(fn) for fn in fns]) # read images\n",
    "y_test = np.expand_dims(y_test, -1) # add channels dimension"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Preprocessing**\n",
    "\n",
    "- Convert to float32\n",
    "- Rescale to 0-1 interval"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def preprocess(ims):\n",
    "    # convert to float32\n",
    "    ims = ims.astype(np.float32)\n",
    "\n",
    "    # scale to 0-1 interval\n",
    "    if ims.max() > 1.0:\n",
    "        ims /= 255.\n",
    "    \n",
    "    print('min: {}, max: {}, shape: {}, type: {}'.format(ims.min(), ims.max(), ims.shape, ims.dtype))\n",
    "    return ims\n",
    "\n",
    "x_train = preprocess(x_train)\n",
    "x_test = preprocess(x_test)\n",
    "\n",
    "y_train = preprocess(y_train)\n",
    "y_test = preprocess(y_test)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Data augmentation**\n",
    "\n",
    "- Generate a large collection of small image/segmentation snippets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def random_snippet(x, y, size=(48,48), rotate=True, flip=True):\n",
    "    '''sample snippets from images. return image tuple (real, segmentation) of size `size` '''\n",
    "    \n",
    "    assert x.shape[:2] == y.shape[:2]\n",
    "    \n",
    "    # get image sample\n",
    "    sample = np.random.randint(0, x.shape[0])\n",
    "    # get x and y dimensions\n",
    "    min_h = np.random.randint(0, x.shape[1]-size[0])\n",
    "    max_h = min_h+size[0]\n",
    "    min_w = np.random.randint(0, x.shape[2]-size[1])\n",
    "    max_w = min_w+size[1]\n",
    "    # extract snippet\n",
    "    im_x = x[sample, min_h:max_h, min_w:max_w, :]\n",
    "    im_y = y[sample, min_h:max_h, min_w:max_w, :]\n",
    "    \n",
    "    # rotate\n",
    "    if rotate:\n",
    "        k = np.random.randint(0,4)\n",
    "        im_x = np.rot90(im_x, k=k, axes=(0,1))\n",
    "        im_y = np.rot90(im_y, k=k, axes=(0,1))\n",
    "        \n",
    "    # flip left-right, up-down\n",
    "    if flip:\n",
    "        if np.random.random() < 0.5:\n",
    "            lr_ud = np.random.randint(0,2) # flip up-down or left-right?\n",
    "            im_x = np.flip(im_x, axis=lr_ud)\n",
    "            im_y = np.flip(im_y, axis=lr_ud)\n",
    "            \n",
    "    \n",
    "    \n",
    "    return (im_x, im_y)\n",
    "\n",
    "def get_random_snippets(x, y, number, size):\n",
    "    snippets = [random_snippet(x, y, size) for i in range(number) ]\n",
    "     \n",
    "    ims_x = np.array([i[0] for i in snippets])\n",
    "    ims_y = np.array([i[1] for i in snippets])\n",
    "    return (ims_x, ims_y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train, Y_train = get_random_snippets(x_train, y_train, number=1000, size=(48,48))\n",
    "X_test, Y_test = get_random_snippets(x_train, y_train, number=100, size=(48,48))\n",
    "\n",
    "print(X_train.shape)\n",
    "print(Y_train.shape)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Plot random snippet**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = np.random.randint(0, X_train.shape[0])\n",
    "fig, ax = plt.subplots(1,2)\n",
    "ax[0].imshow(X_train[n])\n",
    "ax[1].imshow(np.squeeze(Y_train[n]))\n",
    "for a in ax: a.axis('off')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Define a convolutional neural network"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def UNet():\n",
    "    '''U net architecture (down/up sampling with skip architecture)\n",
    "    '''\n",
    "    from keras.layers import Input, Dropout, Activation, BatchNormalization, Conv2D, Concatenate, UpSampling2D\n",
    "    def Conv2DReluBatchNorm(n_filters, kernel_size, strides, inputs):\n",
    "        return Dropout(rate=0.2)(Activation(activation='relu')(BatchNormalization(scale=False)(Conv2D(n_filters, kernel_size, strides=strides, padding='same', kernel_initializer='glorot_normal')(inputs))))\n",
    "\n",
    "    inputs = Input((None, None, 3))\n",
    "    \n",
    "    layer1 = Conv2DReluBatchNorm(32,  (5, 5), (1,1), inputs)    \n",
    "    layer2 = Conv2DReluBatchNorm(64,  (5, 5), (2,2), layer1)\n",
    "    layer3 = Conv2DReluBatchNorm(128, (3, 3), (2,2), layer2)\n",
    "    layer4 = Conv2DReluBatchNorm(256, (3, 3), (2,2), layer3)\n",
    "    layer5 = Conv2DReluBatchNorm(512, (3, 3), (2,2), layer4)\n",
    "\n",
    "    merge6 = Concatenate(axis=-1)([UpSampling2D(size=(2,2))(layer5), layer4])\n",
    "    layer6 = Conv2DReluBatchNorm(256, (3, 3), (1,1), merge6)\n",
    "    \n",
    "    merge7 = Concatenate(axis=-1)([UpSampling2D(size=(2,2))(layer6), layer3])\n",
    "    layer7 = Conv2DReluBatchNorm(128, (3, 3), (1,1), merge7)\n",
    "\n",
    "    merge8 = Concatenate(axis=-1)([UpSampling2D(size=(2,2))(layer7), layer2])\n",
    "    layer8 = Conv2DReluBatchNorm(64, (3, 3), (1,1), merge8)\n",
    "\n",
    "    merge9 = Concatenate(axis=-1)([UpSampling2D(size=(2,2))(layer8), layer1])\n",
    "    layer9 = Conv2DReluBatchNorm(32, (3, 3), (1,1), merge9)\n",
    "\n",
    "    #conv10 = Conv2D(1, (1, 1), strides=(1,1), activation='softmax')(layer9)\n",
    "    outputs = Conv2D(1, (1, 1), strides=(1,1), activation='sigmoid')(layer9)\n",
    "\n",
    "    return (inputs, outputs)\n",
    "\n",
    "\n",
    "def MSDenseNet(width, depth, n_input_channels=1, n_output_channels=1, drop_out=0.0, batch_norm=False):\n",
    "    from keras.layers import Input, Dropout, BatchNormalization, Conv2D, Concatenate\n",
    "    #from keras.regularizers import l2\n",
    "    \n",
    "    def convolution(n_filters, dilation, inputs, drop_out, name=None):\n",
    "        if len(inputs) > 1:\n",
    "            i = Concatenate()(inputs)\n",
    "        else:\n",
    "            i = inputs[0]\n",
    "        c = Conv2D(filters=n_filters, dilation_rate=(dilation, dilation),\n",
    "                                        kernel_size=(3,3), strides=(1,1), padding='same', \n",
    "                                        #kernel_regularizer=l2(0.000),\n",
    "                                        activation='relu', use_bias=True, name=name)(i)\n",
    "        if batch_norm:\n",
    "            c = BatchNormalization()(c)\n",
    "            \n",
    "        if drop_out:\n",
    "            c = Dropout(rate=drop_out)(c)\n",
    "            \n",
    "        return c\n",
    "\n",
    "    inp = Input(shape=(None, None, n_input_channels))\n",
    "    \n",
    "    if batch_norm:\n",
    "        bn = BatchNormalization()(inp)\n",
    "        inputs = [bn]\n",
    "    else:\n",
    "        inputs = [inp]\n",
    "    \n",
    "    for i in range(depth):\n",
    "        for j in range(width): \n",
    "            s_ij = ((i*width + j) % 10) + 1\n",
    "            c = convolution(n_filters=1, dilation=s_ij, inputs=inputs, drop_out=drop_out, name='layer_{}_stride_{}'.format(i, s_ij))\n",
    "            inputs.append(c)\n",
    "        \n",
    "    c = Concatenate()(inputs)\n",
    "    o = Conv2D(filters=n_output_channels, kernel_size=(1,1), padding='same', activation='sigmoid')(c)\n",
    "    \n",
    "    #model.compile(optimizer=Adam(lr=0.005), loss=Fbeta_loss(0.5))# binary_crossentropy)\n",
    "        \n",
    "    return (inp, o)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Compile the network__\n",
    "\n",
    "To compile the network, we need to choose a loss function and an optimizer. \n",
    "\n",
    "As a loss function, we'll use the [Dice coefficient](https://en.wikipedia.org/wiki/S%C3%B8rensen%E2%80%93Dice_coefficient)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from keras import backend as K\n",
    "def dice_coefficient(y_true, y_pred):\n",
    "    \"\"\"\n",
    "    A statistic used for comparing the similarity of two samples. Here binary segmentations.\n",
    "\n",
    "    Args:\n",
    "        y_true (numpy.array): the true segmentation\n",
    "        y_pred (numpy.array): the predicted segmentation\n",
    "\n",
    "    Returns:\n",
    "        (float) returns a number from 0. to 1. measuring the similarity y_true and y_pred\n",
    "    \"\"\"\n",
    "    y_true_f=K.flatten(y_true)\n",
    "    y_pred_f=K.flatten(y_pred)\n",
    "    intersection=K.sum(y_true_f*y_pred_f)\n",
    "    smooth=1.0\n",
    "    return (2*intersection+smooth)/(K.sum(y_true_f)+K.sum(y_pred_f)+smooth)\n",
    "\n",
    "def dice_loss(y_true, y_pred):\n",
    "    return 1-dice_coefficient(y_true, y_pred)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from keras.models import Model\n",
    "from keras import optimizers\n",
    "\n",
    "i, o = MSDenseNet(width=2, depth=25, n_input_channels=3, n_output_channels=1, drop_out=0.2, batch_norm=False)\n",
    "#i, o = UNet()\n",
    "model = Model(inputs=[i], outputs=[o])\n",
    "model.compile(optimizer=optimizers.Adam(lr=0.001), loss=dice_loss, metrics=[dice_coefficient])\n",
    "#model.summary()\n",
    "print(\"Number of parameters: \", model.count_params())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Train the neural network"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "batch_size = 96\n",
    "epochs = 10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "history = model.fit(X_train, Y_train,\n",
    "                      batch_size=batch_size,\n",
    "                      epochs=epochs,\n",
    "                      shuffle=True,\n",
    "                      verbose=1,\n",
    "                      validation_data=(X_test, Y_test))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_history(history, validation=False):\n",
    "    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(10,5), sharex=True)\n",
    "    #fig.tight_layout()\n",
    "    # plot history for loss\n",
    "    ax.plot(history.history['loss'])\n",
    "    if validation:\n",
    "        ax.plot(history.history['val_loss'])\n",
    "    ax.set_title('Loss')\n",
    "    ax.set_ylabel('Loss')\n",
    "    ax.set_ylim(bottom=0.)\n",
    "    ax.set_xlabel('Epoch')\n",
    "    ax.legend(['train', 'test'])\n",
    "    \n",
    "plot_history(history, validation=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "score = model.evaluate(x_test, y_test, verbose=1)\n",
    "print('Test loss:', score[0])\n",
    "print('Test accuracy:', score[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Predict unseen examples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "num=36\n",
    "import time\n",
    "start = time.time()\n",
    "y_pred = model.predict(x_test[:num], verbose=1, batch_size=12)\n",
    "end = time.time()\n",
    "print('Exec time per prediction = {:.3f}'.format((end-start)/num))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Predictions are returned as vectors in one-hot encoding. Or rather, they are the activations of the last layer in the NN."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "y_pred = np.argmax(y_pred, axis=1)\n",
    "y_pred"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gt = np.argmax(y_test[:num], axis=1) \n",
    "gt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_images(x_test[:num], y_pred, gt=gt)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "102px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_position": {
    "height": "501px",
    "left": "0px",
    "right": "1068px",
    "top": "106px",
    "width": "140px"
   },
   "toc_section_display": "block",
   "toc_window_display": true,
   "widenNotebook": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
