{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import pylab as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Deep Learning for Biomedicine with Keras\n",
    "## Validation and Regularization\n",
    "\n",
    "\n",
    "\n",
    "<img src=\"./images/segmentation.png\" width=\"30%\" align=\"right\">\n",
    "\n",
    "\n",
    "[*Walter de Back*](http://walter.deback.net)\n",
    "\n",
    "Institute for Medical Informatics and Biometry (IMB)  \n",
    "*\"Carl Gustav Carus\"* Faculty of Medicine  \n",
    "TU Dresden\n",
    "\n",
    "[GitLab repo](https://gitlab.com/wdeback/dl-keras-tutorial)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question\n",
    "\n",
    "### Difference between statistical modeling and machine learning?\n",
    "\n",
    "- Prediction\n",
    "- Generalization\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "\n",
    "## Generalization\n",
    "\n",
    "<img src=\"./images/regularization.png\" width=\"25%\" align=\"right\">\n",
    "\n",
    "- Goals of machine learning:\n",
    "  - Small training error\n",
    "  - Small gap between training and test error  \n",
    "  (*key difference ML and optimization!*)\n",
    " \n",
    "- Overfitting / underfitting:  \n",
    "  - Underfitting: unable to obtain small training error\n",
    "  - Overfitting: large gap between training and test error.\n",
    "\n",
    "- Generalization:  \n",
    "  *Ability to perform well on previously unobserved inputs*\n",
    "\n",
    "\n",
    "\n",
    "Chapter 5 of [Goodfellow at el., Deep Learning book](http://www.deeplearningbook.org/contents/ml.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Bias variance tradeoff\n",
    "\n",
    "<img src=\"./images/bias-variance-tradeoff2.png\" width=\"25%\" align=\"right\">\n",
    "\n",
    "- Bias: an error from erroneous assumptions in the learning algorithm. \n",
    "    - High bias: danger of underfitting.\n",
    "- Variance: an error from sensitivity to small fluctuations in the training set. \n",
    "   - High variance: danger of overfitting."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Bias variance tradeoff\n",
    "\n",
    "<img src=\"./images/bias-and-variance.jpg\" width=\"25%\" align=\"right\">\n",
    "\n",
    "- Bias: an error from erroneous assumptions  \n",
    "  in the learning algorithm. \n",
    "   - High bias: danger of underfitting.\n",
    "- Variance: an error from sensitivity to small  \n",
    "  fluctuations in the training set. \n",
    "   - High variance: danger of overfitting.\n",
    "   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to prevent overfitting?\n",
    "\n",
    "- Crossvalidation\n",
    "- Regularization\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cross-validation\n",
    "\n",
    "- Hold out\n",
    "\n",
    "- K-fold cross-validation\n",
    "\n",
    "- Leave-one-out cross-validation\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Regularization\n",
    "\n",
    "- Any technique that aims at making  \n",
    "  the model generalize better, i.e.  \n",
    "   produce better results on the test set\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- Regularization methods:\n",
    "  - via data:\n",
    "    - data augmentation: random transformations of input data\n",
    "    - [dropout](http://jmlr.org/papers/volume15/srivastava14a/srivastava14a.pdf)\n",
    "    - batch normalization: normalize internal activations\n",
    "    - train on adversarial examples\n",
    "  - via model (network architecture):\n",
    "    - weight sharing: param reduction (CNN)\n",
    "    - activation functions: ReLU allow more expressiveness \n",
    "    - stochastic models: [Dropout](http://jmlr.org/papers/volume15/srivastava14a/srivastava14a.pdf)\n",
    "    - [multitask learning](http://www.cs.cornell.edu/~caruana/mlj97.pdf): optimize multiple goals with shared representation \n",
    "  - via loss:\n",
    "    - Dice loss: robust to class imbalance\n",
    "  - via regularization term:\n",
    "    - weight decay: penalty on weight value\n",
    "  - via optimization:\n",
    "    - weight initialization: prevent vanishing or exploding gradients (Glorot/He)\n",
    "    - update methods: momentum, learning rate schedules, etc.\n",
    "    - early stopping: stop before start overfitting\n",
    " \n",
    "More info: ([Kukačka et al., 2017](https://arxiv.org/pdf/1710.10686.pdf))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dropout\n",
    "\n",
    "<img src=\"./images/dropout.png\" width=\"25%\" align=\"right\">\n",
    "\n",
    "- Randomly drop (25%-50%) neurons from network during training.\n",
    "\n",
    "  - Related to ensemble models: effectively training many models in parallel ([Hara et al., 2017](https://arxiv.org/abs/1706.06859)).\n",
    "  - Related to Bayesian NNs: weights are Bernoulli distribution ([Gal et al. 2016](https://arxiv.org/abs/1506.02142)) \n",
    "\n",
    "(+) Simple and effective method of regularization.  \n",
    "(+) Forces a neural network to learn more robust features that are useful in conjunction with many different random subsets of the other neurons.  \n",
    "(-) Dropout requires more iterations to converge.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Batch normalization\n",
    "\n",
    "<img src=\"./images/DeepNN.png\" width=\"25%\" align=\"right\">\n",
    "\n",
    "- Intuition: \n",
    "  - similar to input normalization, normalize hidden activations \n",
    "\n",
    "- Covariate shift:\n",
    "  - from perspective of hidden layer, input distribution shifts all the time\n",
    "  - normalize \n",
    "\n",
    "\n",
    "\n",
    "More info: [Andrew Ng's video @ Coursera](https://www.coursera.org/learn/deep-neural-network/lecture/81oTm/why-does-batch-norm-work) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Batch normalization\n",
    "\n",
    "<img src=\"./images/DeepNN.png\" width=\"25%\" align=\"right\">\n",
    "<img src=\"./images/BN_redcats.png\" width=\"25%\" align=\"left\">\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Batch normalization\n",
    "\n",
    "<img src=\"./images/DeepNN.png\" width=\"25%\" align=\"right\">\n",
    "<img src=\"./images/BN_redcats.png\" width=\"25%\" align=\"left\">\n",
    "<img src=\"./images/BN_blackcats.png\" width=\"25%\" align=\"right\">\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Batch normalization\n",
    "\n",
    "**Mean** over minibatch $\\mathcal{B}={x_{i...n}}$:  \n",
    "$\\mu_\\mathcal{B} = \\frac{1}{n}\\sum_{i=0}{n}x_i$\n",
    "\n",
    "**Variance**:  \n",
    "$\\sigma_\\mathcal{B}^2 = \\frac{1}{n}\\sum_{i=0}{n}(x_i-\\mu_\\mathcal{B})^2$\n",
    "\n",
    "**Normalization**:  \n",
    "$\\hat z_i = \\frac{x_i-\\mu_\\mathcal{B}}{\\sqrt{\\sigma_\\mathcal{B}^2 + \\epsilon}}$\n",
    "\n",
    "**Scale and shift**:  \n",
    "$y_i = \\gamma \\hat z_i + \\beta$  \n",
    "\n",
    "where $\\gamma$ and $\\beta$ are **learned parameter**\n",
    "\n",
    "(+) Reduces training time\n",
    "(+) Train deeper networks\n",
    "(-) Not stable for small batch sizes\n",
    "(-) Different calculation between train and test phase\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Batch normalization\n",
    "\n",
    "<img src=\"./images/instance_norm.png\" width=\"25%\" align=\"right\">\n",
    "\n",
    "Variants of batch normalization:\n",
    "- [Layer normalization](https://arxiv.org/abs/1607.06450)\n",
    "  - compute moments for all neurons in a layer, not per neuron\n",
    "  - same during training and test phase\n",
    "  - used for recurrent networks\n",
    "  \n",
    "- [Instance/contrast normalization](https://arxiv.org/abs/1607.08022)\n",
    "  - apply normalization per data instance, not over whole batch\n",
    "  - apply during training and test\n",
    "  - dramatically improves style transfer\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
