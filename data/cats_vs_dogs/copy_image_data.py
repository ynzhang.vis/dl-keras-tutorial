import os,glob
import numpy as np
from shutil import copyfile

num_files = 200

all_ims = glob.glob('../train/dog*.jpg')
all_ims = np.array(all_ims)
print(all_ims)
random_elements = np.random.choice( range(len(all_ims)), replace=False, size=num_files)
img_files = all_ims[ random_elements ]

print(random_elements)
print(img_files)

for i, img in enumerate(img_files):
        print('.', end='')
        dest = 'validate/dogs/{:05d}.jpg'.format(i)
        #print(img, dest)
        copyfile(img, dest)
